#!/usr/bin/python3
import turtle

def drawLandscape(tur):
    tur.up()
    tur.goto(-250, 250)
    tur.down()
    tur.color("#000000", "#5AAAFB")
    tur.begin_fill()
    tur.goto(250, 250)
    tur.goto(250, 0)
    tur.goto(-250, 0)
    tur.goto(-250, 250)
    tur.end_fill()
    
    tur.color("#000000", "#969696")
    tur.begin_fill()
    tur.up()
    tur.goto(-250, 100)
    tur.down()
    tur.goto(-200, 180)
    tur.goto(-180, 150)
    tur.goto(-100, 200)
    tur.goto(-50, 110)
    tur.goto(-40, 150)
    tur.goto(0, 130)
    tur.goto(20, 150)
    tur.goto(60, 100)
    tur.goto(150, 165)
    tur.goto(230, 120)
    tur.goto(250, 100)
    tur.goto(250, 0)
    tur.goto(-250, 0)
    tur.goto(-250, 100)
    tur.end_fill()

    tur.up()
    tur.goto(-250, 0)
    
    tur.color("#000000", "#842705")
    tur.begin_fill()
    tur.down()
    tur.goto(-250, -250)
    tur.goto(250, -250)
    tur.goto(250, 0)
    tur.goto(-250, 0)
    tur.end_fill()
    
def Intro(tur):
    turLandscape = turtle.Turtle()
    drawLandscape(turLandscape)

    tur.hideturtle()
    tur.up()
    tur.goto(-160, 150)
    tur.write("Missile Command", font=("mononoki Nerd Font", 30))
    tur.goto(-80, 130)
    tur.write("Menu [Return]", font=("mononoki Nerd Font", 15))

    return True
