#+title: Missile Command
* Table of Contens
  - [[*Missile Command][Missile Command]]
  - [[*Dependence][Dependence]]
  - [[*Play the game][Play the game]]
  - [[*Todo][Todos]]

* Missile Command
  This game is inspired by the classic game "Missile Command". It is programmed in Python and all the graphics are drawn with the turtle module. The game is turn based after one shoot with the missile the turn ends. After each turn all UFOs will be continueing there landing. Will three UFOs survive and be able to land the, game is over. Shoot as many UFOs as you can to get the highest score!

  #+CAPTION: Missile Command
  [[./game.jpg]]

** Dependence
   Just Python has to be installed.

** Play the game
   The goal of the game is to destroy all enemy UFOs and reach the highest score. To do so, hit them with the missile. To controll the missile:
   + "left arrow key" to slope the missile left
   + "right arrow key" to slope the missile right
   + "space key" to lunch the missile
   + "ECS" quit the game   

** TODO Todo [2/4]
   - [X] Highscore
   - [ ] Change the waves of the UFOs
   - [ ] Nicer landscape
   - [X] revise collision detection
