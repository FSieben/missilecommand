#!/usr/bin/python3
"""MS_Spielen.py. Main function for the game."""
import turtle
import math
import random
import ms_intro

random.seed()

angle = 0
turText = 0
turTextLeben = 0
turTextPunkte = 0
turTextWinkel = 0
turStation = 0
state = 0
timerZaehler = 0
escPressed = False
spacePressed = False
ufos = []
gelandeteUfos = []
level = 0
punkte = 0
leben = 0


class classUfo:
    """Ufo class."""

    def __init__(self, maxRunden):
        """Init function for classUfo."""
        self.startPos = [random.randrange(-250, 230), 200]
        self.aktuellePos = []
        self.endPos = [random.randrange(-250, 230), -230]
        self.turUfo = turtle.Turtle()
        self.turUfo.up()
        self.getroffen = False
        self.maxRunden = maxRunden
        self.aktuelleRunde = 0
        self.gelandet = False
        self.turUfo.shape("myUfo")
        self.entfernung = 0

    def zeichneUfo(self, posX, posY):
        """Draw all ufos."""
        if self.getroffen == False:
            if self.aktuelleRunde == self.maxRunden:
                self.gelandet = True

            self.turUfo.hideturtle()
            deltaPosX = (self.endPos[0] - self.startPos[0])*(self.aktuelleRunde/self.maxRunden)
            deltaPosY = (self.endPos[1] - self.startPos[1])*(self.aktuelleRunde/self.maxRunden)
            self.aktuellePos = [self.startPos[0]+deltaPosX, self.startPos[1]+deltaPosY]
            self.turUfo.goto(self.aktuellePos)
            self.turUfo.showturtle()
            self.entfernung = math.sqrt((posX - self.aktuellePos[0])**2 + (posY - self.aktuellePos[1])**2)

    def loescheUfo(self):
        """Delete all ufos."""
        self.turUfo.hideturtle()
        self.turUfo.clear()

    def naechsteRunde(self):
        """Calculate next round."""
        if self.aktuelleRunde < self.maxRunden:
            self.aktuelleRunde += 1
        else:
            self.gelandet = True
            self.turUfo.shape("Gelandet")

    def treffer(self, winkel):
        """Calculate if the ufo is hit."""
        angleToX = (90 - angle)/180 * math.pi
        tanAlpha = math.tan(angleToX)

        # y(x)=angletoX*x-220
        # ==>> x=(y(x)+220)/angleToX
        # Calculate if hit
        turUfoPos = self.turUfo.position() # turUfoPos[0] = xPos / turUfoPos[1] = yPos
        ufoHitY = tanAlpha * turUfoPos[0] - 220
        ufoHitX = (turUfoPos[1] + 220) / tanAlpha
        if (turUfoPos[1] <= ufoHitY+12 and turUfoPos[1] >= ufoHitY-7) or (turUfoPos[0] <= ufoHitX+12 and turUfoPos[0] >= ufoHitX-12):
            self.getroffen = True

# Define function
n = 30
# (cx,cy): center of ellipse, a: width, b:height, angle: tilt


def ellipse(tur,cx,cy,a,b,angle):
    """Draw an ellipse."""
    # draw the first point with pen up
    t = 0
    xStart = cx + a*math.cos(t)*math.cos(math.radians(angle))-b*math.sin(t)*math.sin(math.radians(angle))
    yStart = cy + a*math.cos(t)*math.sin(math.radians(angle))+b*math.sin(t)*math.cos(math.radians(angle))
    tur.speed(0)
    tur.up()
    tur.goto(xStart,yStart)
    tur.down()
    tur.color('red')
    # draw the rest with pen down
    for i in range(n):
        x = cx + a*math.cos(t)*math.cos(math.radians(angle))-b*math.sin(t)*math.sin(math.radians(angle))
        y = cy + a*math.cos(t)*math.sin(math.radians(angle))+b*math.sin(t)*math.cos(math.radians(angle))
        tur.goto(x, y)
        t += 2*math.pi/n
    tur.goto(xStart, yStart)


def angleUp():
    """Angle up."""
    global angle
    angle += 1
    # turTextLeben.clear()
    # turTextLeben.write(f"Leben: {leben}", font = ("mononoki Nerd Font", 15))
    # turTextPunkte.clear()
    # turTextPunkte.write(f"{punkte:6}", font = ("mononoki Nerd Font", 15))
    turTextWinkel.clear()
    # turTextWinkel.write(f"{angle:4}", font = ("mononoki Nerd Font", 10))


def angleDown():
    """Angle down."""
    global angle
    angle -= 1
    # turTextLeben.clear()
    # turTextLeben.write(f"Leben: {leben}", font = ("mononoki Nerd Font", 15))
    # turTextPunkte.clear()
    # turTextPunkte.write(f"{punkte:6}", font = ("mononoki Nerd Font", 15))
    turTextWinkel.clear()
    # turTextWinkel.write(f"{angle:4}", font = ("mononoki Nerd Font", 10))


def key_escape():
    """Key escape."""
    global escPressed
    escPressed = True


def key_space():
    """Key space."""
    global spacePressed
    spacePressed = True


def neuesUfo():
    """Draw new ufo."""
    # ufoAnzahl = 0
    # if level == 1:
    #     if len(ufos) < 1:
    #         ufos.append(classUfo(10*100/(level+1)))
    # elif level == 2:
    #     while len(ufos) < 2:
    #         ufos.append(classUfo(10*100/(level+1)))

    while len(ufos) < level:
        ufos.append(classUfo(10*30/(level+1)))


def zeichneUfos():
    """Draw ufos."""
    for ufo in ufos:
        ufo.zeichneUfo(0, -220)


def ufoShape(window, tur):
    """Define the shape of the ufo."""
    shape = turtle.Shape("compound")
    shapeGelandet = turtle.Shape("compound")
    tur.hideturtle()
    tur.up()
    tur.goto(10, 0)
    tur.begin_poly()
    ellipse(tur, 0, 0, 10, 5, 0)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#0000ff', '#000000')
    shapeGelandet.addcomponent(tur.get_poly(), '#0000ff', '#000000')
    tur.up()
    tur.goto(-5, 5)
    tur.begin_poly()
    tur.down()
    tur.circle(-5, 180)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#808080', '#000000')
    shapeGelandet.addcomponent(tur.get_poly(), '#808080', '#000000')
    window.register_shape("myUfo", shape)
    tur.up()
    tur.goto(-5, -3)
    tur.begin_poly()
    tur.down()
    tur.goto(-8, -7)
    tur.end_poly()
    shapeGelandet.addcomponent(tur.get_poly(), '#000000', '#000000')
    tur.up()
    tur.goto(5, -3)
    tur.begin_poly()
    tur.down()
    tur.goto(8, -7)
    tur.end_poly()
    shapeGelandet.addcomponent(tur.get_poly(), '#000000', '#000000')
    window.register_shape("Gelandet", shapeGelandet)
    tur.clear()


def raketeShape(window, tur):
    """Define the shape of the rocket."""
    shape = turtle.Shape("compound")
    shapeGestartet = turtle.Shape("compound")

    tur.up()
    tur.goto(-2, 6)
    tur.down()
    tur.begin_poly()
    tur.goto(2, 6)
    tur.goto(2, -6)
    tur.goto(-2, -6)
    tur.goto(-2, 6)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#000000', '#000000')
    shapeGestartet.addcomponent(tur.get_poly(), '#000000', '#000000')
    tur.up()
    tur.goto(-2, 6)
    tur.down()
    tur.begin_poly()
    tur.goto(0, 11)
    tur.goto(2, 6)
    tur.goto(-2, 6)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#FF0000', '#FF0000')
    shapeGestartet.addcomponent(tur.get_poly(), '#FF0000', '#FF0000')
    tur.up()
    tur.goto(-4, -6)
    tur.down()
    tur.begin_poly()
    tur.goto(-4, -6)
    tur.goto(-3, -2)
    tur.goto(-3, -6)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#969696', '#969696')
    shapeGestartet.addcomponent(tur.get_poly(), '#969696', '#969696')
    tur.up()
    tur.goto(4, -6)
    tur.down()
    tur.begin_poly()
    tur.goto(4, -6)
    tur.goto(3, -2)
    tur.goto(3, -6)
    tur.end_poly()
    shape.addcomponent(tur.get_poly(), '#969696', '#969696')
    shapeGestartet.addcomponent(tur.get_poly(), '#969696', '#969696')
    tur.up()
    tur.goto(4, -6)
    tur.down()
    tur.begin_poly()
    tur.goto(6, -9)
    tur.goto(-6, -9)
    tur.goto(-4, -6)
    tur.end_poly()
    shapeGestartet.addcomponent(tur.get_poly(), '#FFFF11', '#FFFF11')
    window.register_shape("Rakete", shape)
    window.register_shape("RaketeGestartet", shapeGestartet)
    tur.clear()


def spielen(window):
    """Start game."""
    global angle, state, escPressed, ufos, spacePressed, turText, turStation, punkte
    global timerZaehler, level, gelandeteUfos, leben, turTextLeben, turTextPunkte, turTextWinkel

    if state == 0:
        turLandschaft = turtle.Turtle()
        turLandschaft.speed(0)
        ms_intro.drawLandscape(turLandschaft)

        leben = 3
        turTextLeben = turtle.Turtle()
        turTextLeben.hideturtle()
        turTextLeben.up()
        turTextLeben.goto(-230, 220)
        turTextLeben.write(f"Leben: {leben}", font=("mononoki Nerd Font", 15))

        punkte = 0
        turTextPunkte = turtle.Turtle()
        turTextPunkte.hideturtle()
        turTextPunkte.up()
        turTextPunkte.goto(160, 220)
        turTextPunkte.write(f"{punkte:6}", font=("mononoki Nerd Font", 15))

        turTextWinkel = turtle.Turtle()
        turTextWinkel.hideturtle()
        turTextWinkel.up()
        turTextWinkel.goto(-15, -248)
        # turTextWinkel.write(f"{angle:4}", font = ("mononoki Nerd Font", 10))

        turStation = turtle.Turtle()
        turStation.up()
        turStation.goto(0, -220)
        # turStation.down()
        turStation.showturtle()

        tur = turtle.Turtle()
        ufoShape(window, tur)
        raketeShape(window, tur)
        turStation.shape("Rakete")

        leben = 3

        state = 10
    elif state == 10:
        '''
        Spiel starten. Die Ufos werden gesetzt. Abhängig nach dem Level wird die Anzahl der Ufos bestimmt.
        Mit dem Level variiert auch die Geschwindigkeit der Ufos.
        '''
        if len(ufos) < 1:
            level += 1
            neuesUfo()

        zeichneUfos()
        state = 15
        if escPressed:
            escPressed = False
            state = 100
    elif state == 15:
        turStation.setheading(angle)
        if spacePressed:
            spacePressed = False
            ersterDurchgang = True
            for ufo in ufos:
                # Ufo getroffen?
                ufo.treffer(angle)
                # Nur das Ufo treffen, welches der Station am nächsten ist!
                if ufo.getroffen:
                    if ersterDurchgang:
                        tempUfo = ufo
                        ersterDurchgang = False
                    elif ufo.entfernung < tempUfo.entfernung:
                        tempUfo.getroffen = False
                        tempUfo = ufo
                # Die Ufos eine Runde weiter setzen. Beim nächsten Zeichnen werden sie weiter fliegen.
                ufo.naechsteRunde()

            # Schuss zeichnen
            # Wenn nicht getroffen, dann bis zum Rand zeichen
            # locals() schaut nach, ob es die Variable gibt!
            turStation.shape("RaketeGestartet")
            turStation.speed(2)
            if 'tempUfo' in locals():
                # tempUfo gibt es -> Ein Ufo wurde getroffen.
                turStation.goto(tempUfo.aktuellePos)
                # loescheUfo()
                tempUfo.loescheUfo()
                ufos.remove(tempUfo)
                punkte += 10
                turTextPunkte.clear()
                turTextPunkte.write(f"{punkte:6}", font=("mononoki Nerd Font", 15))
            else:
                # tempUfo gibt es nicht -> Kein Ufo wurde getroffen.
                tanAlpha = math.tan((90 - angle)/180 * math.pi)
                if angle >= -31 and angle <= 31:
                    tempY = 210
                    tempX = (tempY+220)/tanAlpha
                else:
                    if angle <= -31:
                        tempX = -250
                    elif angle >= 31:
                        tempX = 250
                    tempY = tanAlpha*tempX-220
                turStation.goto(tempX, tempY)
            turStation.ht()
            turStation.shape("Rakete")

            # Gelandete Ufos umkopieren und so merken. Für jedes gelandete Ufo ein Leben abziehen.
            for ufo in ufos:
                if ufo.gelandet:
                    gelandeteUfos.append(ufo)
                    ufos.remove(ufo)
                    leben -= 1
                    turTextLeben.clear()
                    turTextLeben.write(f"Leben: {leben}", font=("mononoki Nerd Font", 15))

            timerZaehler = 0
            if leben <= 0:
                state = 25
            else:
                state = 20

        if escPressed is True:
            escPressed = False
            state = 100
    elif state == 20:
        ''' Warte 20 Durchläufe (1 Sekunde), dann den Schuss löschen und eventuell neue
        Ufos generieren. Sonst weitere Versuche bis keine Ufos mehr da sind.'''
        if timerZaehler < 20:
            timerZaehler += 1
        else:
            turStation.clear()
            # turStation.up()
            turStation.goto(0, -220)
            turStation.showturtle()
            # turStation.down()
            timerZaehler = 0
            state = 10
    elif state == 25:  # Game over
        print("Game Over")
        state = 100
    elif state == 100:
        state = 0
        angle = 0
        ufos = []
        level = 0
        return 100

    # Set key events
    window.onkey(angleUp, "Right")
    window.onkey(angleDown, "Left")
    window.onkey(key_escape, "Escape")
    window.onkey(key_space, "space")

    return 0
