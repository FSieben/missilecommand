#!/usr/bin/python3
"""Load and save the highscore."""
import json


class class_highscore:
    """Edit the highscore."""

    def __init__(self, file_name="highscore.json"):
        """Init function for highscore."""
        self.file_name = file_name

        try:
            with open(self.file_name, "r") as f:
                self.highscore = json.load(f)
        except:
            self.highscore = [["AAA", 1000], ["BBB", 100], ["CCC", 10], ["DDD", 10], ["EEE", 10], ["FFF", 10], ["GGG", 10], ["HHH", 10], ["III", 10], ["JJJ", 10]]
            with open(file_name, "w") as f:
                json.dump(self.highscore, f)

    def add(self, score):
        """Add a new score to the highscore."""
        """score = ["AAA", 123]"""
        self.highscore.sort(key=lambda e: e[1], reverse=True)
        if self.highscore[9][1] < score[1]:
            self.highscore[9] = score
            self.highscore.sort(key=lambda e: e[1], reverse=True)
            return 0
        else:
            return 1

    def save(self):
        """Save highscore into file."""
        with open(self.file_name, "w") as f:
            json.dump(self.highscore, f)
        return 0

class class_name:
    """Name for the highscore."""

    def __init__(self):
        """Init function for name."""
        self.name = ["A", "A", "A"]
        self.position = 0

    def get_string(self):
        """Return the name string."""
        return self.name[0] + self.name[1] + self.name[2]

    def get(self):
        """Return the name as a tuple."""
        return self.name

    def set(self, name_str):
        """Set the name directly."""
        self.name = name_str

    def letter_up(self):
        """Letter up."""
        if self.name[self.position].find("Z"):
            self.name[self.position] = chr(ord(self.name[self.position])+1)

    def letter_down(self):
        """Letter down."""
        if self.name[self.position].find("A"):
            self.name[self.position] = chr(ord(self.name[self.position])-1)

    def letter_left(self):
        """Letter left."""
        if self.position > 0:
            self.position -= 1

    def letter_right(self):
        """Letter right."""
        if self.position < 2:
            self.position += 1

    def letter_finish(self):
        """Letter finish."""
        self.name = self.name_user_input
        self.position = 3


def showHighscore(tur, pos, highscore):
    """Draw the highscore on the screen."""
    tur.ht()
    tur.up()
    tur.goto(pos)
    for score in highscore:
        tur.write(f"{score[0]}" + "." * (10 - len(str(score[1]))) + f"{score[1]}", font=("mononoki Nerd Font", 20))
        pos[1] -= 25
        tur.goto(pos)


def get_name(window, tur, pos, score):
    """Get the name of the Player."""
    pass
    # global name_old

    # window.onkey(letter_right, "Right")
    # window.onkey(letter_left, "Left")
    # window.onkey(letter_up, "Up")
    # window.onkey(letter_down, "Down")
    # window.onkey(letter_finish, "Return")

    # tur.ht()
    # tur.up()
    # tur.goto(pos)
    # list_1 = set(name)
    # list_2 = set(name_old)
    # if list_1 != list_2:
    #     name_old = []
    #     for item in name:
    #         name_old.append(item)
    #     tur.clear()
    #     tur.write(f"{name[0]}" + f"{name[1]}" + f"{name[2]}" + "." * (10 - len(str(score))) + f"{score}", font=("mononoki Nerd Font", 20))
