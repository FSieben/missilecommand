#!/usr/bin/python3
"""Main function."""
import turtle
import ms_intro
import ms_menue
import ms_spielen
import ms_highscore


window = turtle.Screen()
# Set mode. "logo" = turtle shows to nord, angle clockwise
turtle.mode("logo")

# Setup the new generated window
window.setup(width=500, height=500, startx=1300, starty=200)
window.title("Missle Command")

gameState = 0
tur = turtle.Turtle()
turLandscape = turtle.Turtle()
KEY_DOWN, KEY_LEFT, KEY_RETURN, KEY_RIGHT, KEY_UP, KEY_ESC = 0, 0, 0, 0, 0, 0


def key_return():
    """Key return."""
    global gameState
    global KEY_RETURN
    if gameState == 1:
        gameState = 5
    elif gameState == 17 or gameState == 23:
        KEY_RETURN = 1


def key_escape():
    """Key escape."""
    global gameState
    global KEY_ESC
    if gameState == 0 or gameState == 1:
        gameState = 30
    elif gameState == 10 or gameState == 11:
        gameState = 30
    elif gameState == 17 or gameState == 23:
        KEY_ESC = 1


def key_eins():
    """Key one."""
    global gameState
    if gameState == 11:
        gameState = 15


def key_zwei():
    """Key two."""
    global gameState
    if gameState == 11:
        gameState = 16


def key_up():
    """Key Up."""
    global gameState
    global KEY_UP
    if gameState == 23:
        KEY_UP = 1


def key_down():
    """Key Down."""
    global gameState
    global KEY_DOWN
    if gameState == 23:
        KEY_DOWN = 1


def key_right():
    """Key Right."""
    global gameState
    global KEY_RIGHT
    if gameState == 23:
        KEY_RIGHT = 1


def key_left():
    """Key Left."""
    global gameState
    global KEY_LEFT
    if gameState == 23:
        KEY_LEFT = 1


def clearScreen(window):
    """Clear the screen."""
    window.clearscreen()
    set_keys(window)
    

def set_keys(window):
    """Set the key bindings."""
    window.onkey(key_return, "Return")
    window.onkey(key_escape, "Escape")
    window.onkey(key_eins, "1")
    window.onkey(key_zwei, "2")
    window.onkey(key_up, "Up")
    window.onkey(key_down, "Down")
    window.onkey(key_left, "Left")
    window.onkey(key_right, "Right")

highscore = ms_highscore.class_highscore()
name = ms_highscore.class_name()

def Hauptmenue():
    """Hauptmenue is the main function for the game."""
    global gameState
    global KEY_DOWN, KEY_LEFT, KEY_RETURN, KEY_RIGHT, KEY_UP, KEY_ESC

    if gameState == 0:
        ms_intro.Intro(tur)
        gameState = 1
    elif gameState == 1:
        pass
    elif gameState == 5:
        tur.reset()
        gameState = 10
    elif gameState == 10:
        ms_menue.menue(tur)
        gameState = 11
    elif gameState == 11:
        pass
    elif gameState == 15:
        clearScreen(window)
        gameState = 20
    elif gameState == 16:
        set_keys(window)
        tur.reset()
        ms_highscore.showHighscore(tur, [-100, 100], highscore.highscore)
        gameState = 17
    elif gameState == 17:
        if KEY_ESC == 1 or KEY_RETURN == 1:
            KEY_ESC, KEY_RETURN = 0, 0
            gameState = 5
    elif gameState == 20:
        if ms_spielen.spielen(window) == 100:
            set_keys(window)
            gameState = 23
    elif gameState == 23:
        """Save highscore."""
        if highscore.highscore[9][1] < ms_spielen.punkte:
            tur.up()
            tur.goto(-100, 100)
            if KEY_UP == 1:
                name.letter_up()
                tur.clear()
                tur.write(f"{name.get_string()}" + "." * (10 - len(str(ms_spielen.punkte))) + f"{ms_spielen.punkte}", font=("mononoki Nerd Font", 20))
                tur.goto(-100, 80)
                tur.write(" " * name.position + "^", font=("mononoki Nerd Font", 20))
                KEY_UP = 0
            elif KEY_DOWN == 1:
                name.letter_down()
                tur.clear()
                tur.write(f"{name.get_string()}" + "." * (10 - len(str(ms_spielen.punkte))) + f"{ms_spielen.punkte}", font=("mononoki Nerd Font", 20))
                tur.goto(-100, 80)
                tur.write(" " * name.position + "^", font=("mononoki Nerd Font", 20))
                KEY_DOWN = 0
            elif KEY_RIGHT == 1:
                name.letter_right()
                tur.clear()
                tur.write(f"{name.get_string()}" + "." * (10 - len(str(ms_spielen.punkte))) + f"{ms_spielen.punkte}", font=("mononoki Nerd Font", 20))
                tur.goto(-100, 80)
                tur.write(" " * name.position + "^", font=("mononoki Nerd Font", 20))
                KEY_RIGHT = 0
            elif KEY_LEFT == 1:
                name.letter_left()
                tur.clear()
                tur.write(f"{name.get_string()}" + "." * (10 - len(str(ms_spielen.punkte))) + f"{ms_spielen.punkte}", font=("mononoki Nerd Font", 20))
                tur.goto(-100, 80)
                tur.write(" " * name.position + "^", font=("mononoki Nerd Font", 20))
                KEY_LEFT = 0
            elif KEY_RETURN == 1 and not name.position == 3:
                name.position = 3
                highscore.add((name.get_string(), ms_spielen.punkte))
                highscore.save()
                tur.clear()
                ms_highscore.showHighscore(tur, [-100, 100], highscore.highscore)
                KEY_RETURN = 0
            if name.position == 3:
                if KEY_RETURN == 1 or KEY_ESC == 1:
                    clearScreen(window)
                    name.position = 0
                    KEY_RETURN = 0
                    KEY_ESC = 0
                    gameState = 25
        else:
            gameState = 25
    elif gameState == 25:
        tur.speed(0)
        ms_intro.drawLandscape(tur)
        gameState = 10
    elif gameState == 30:
        window.bye()

    window.ontimer(Hauptmenue, 50)


# Set window in focus. To get key events!
window.listen()
# Set keys
clearScreen(window)

Hauptmenue()

# Start turtle mainloop. To let the window alife.
window.mainloop()
