#!/usr/bin/python3
import turtle

def menue(tur):
    tur.hideturtle()
    tur.up()
    tur.goto(-100, 100)
    tur.write("Start     [1]\nHighscore [2]\nQuit      [Esc]", font=("mononoki Nerd Font", 20))

    return True
